<?php

/*
|--------------------------------------------------------------------------
| Application Routes  
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/laravel_local', function () {        
	echo "HELLO WORLD TESTING.ggg..999"; 
    return view('welcome');      
});

Route::get('/laravel_local/test',function() {       
	echo "Testing...";
});


Route::get('/laravel_local/demo',function() {
	echo "Demo Goes Here... 444";
}); 


Route::get('/laravel_local/gg',function() {
	echo "GGGGGGGG...GGGGGG...FFF";
}); 
